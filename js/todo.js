$(function() {

    $("#button").click(refreshPlayerList);
    document.getElementById("10x10").onclick = function(){lancerPartie(10,10,10);};
    document.getElementById("15x15").onclick = function(){lancerPartie(15,15,25);};
    document.getElementById("20x20").onclick = function(){lancerPartie(20,20,50);};
    document.getElementById("btStop").onclick = function(){arreterPartie();};


    var nbWin;
    var nbAction;
    var nbCase;

    //fonction qui s'occupe du lancement d'une nouvelle partie composé d'une grille de hauteurxlargeur cases et d'un nombre de bombes
    function lancerPartie(hauteur, largeur, nbBombes){
      $("#currenttask").empty();
      var partie = new Partie(hauteur, largeur, nbBombes);
      genPlateau(partie);
      nbWin = compter();
      nbAction = 0;
      nbCase = document.getElementsByClassName("case").length;
      resetChrono();
      startChrono();
      // document.getElementById("10x10").type = "hidden";
      // document.getElementById("15x15").type = "hidden";
      document.getElementById("10x10").disabled = true;
      document.getElementById("15x15").disabled = true;
      document.getElementById("20x20").disabled = true;
      let btn = document.getElementById("del");
      btn.bombes = nbBombes;
      btn.alt = btn.bombes;
    }

    //fonction qui permet de switcher la couleur du drapeau dans la page
    function drapeau(){
      let btn = document.getElementById("flag");
      btn.flag = !btn.flag;
      if (btn.flag){
        btn.src="img/flag-green.png";
      }
      else{
        btn.src="img/flag.png";
      }
    }

    //fonction qui permet d'arréter une partie soit lorsqu'on perd soit lorsqu'on veut tout simplement arréter
    function arreterPartie(){
      discoverAll();
      stopChrono();
      document.getElementById("10x10").disabled = false;
      document.getElementById("15x15").disabled = false;
      document.getElementById("20x20").disabled = false;
    }

    //fonction qui est activé lorsqu'on gagne une partie, elle affiche un message et enregistre le score du joueur
    function win(){
      stopChrono();
      let res = chronometer.value;
      $("#currenttask").empty();
      let para1 = document.createElement("h1");
      let texte1 = document.createTextNode("YOU WIN ! GOOD JOB ! \n YOUR SCORE IS : "+res);
      para1.appendChild(texte1);
      $("#currenttask").append(para1);
      $("#currenttask").append($('<span> Votre Pseudo ? <input type="text" id="name"><br></span>'))
      $("#currenttask").append($('<input type="button" id="valid" class="btn btn-success" value=Valider onclick=saveNewPlayer()>'))
      document.getElementById("10x10").disabled = false;
      document.getElementById("15x15").disabled = false;
      document.getElementById("20x20").disabled = false;
    }

    //fonction qui découvre le contenu d'un bouton (appelé lorsqu'on clique sur une case)
    function discover(button){
      // let button = document.getElementById("btn"+i+":"+j);
      let c = button.cellule;
      c.discover();
      button.disabled = true;
      button.value = c.contenu+"";
      if (c.suspect){
        let cpt = document.getElementById("del");
        cpt.bombes+=1;
        cpt.alt = cpt.bombes+"";
      }
      if (c.estBombe()){
        button.value = "💣";
        arreterPartie();
      }
      nbAction += 1;
      if (nbAction == nbWin){
        win();
      }
      else {
        if (button.value == "0"){
          for (let v of c.voisins){
            if (!v.decouvert){
              discover(document.getElementById("btn"+v.line+":"+v.column));
            }
          }
        }
      }
    }

    //fonction qui compte le nombre de bombes dans la grille pour déterminer lorsque la partie est gagné
    function compter(){
      cpt = 0;
      let liste = document.getElementsByClassName("case");
      for (let i=0; i< liste.length; i++){
        if (!liste[i].cellule.estBombe()){
          cpt+=1;
        }
      }
      return cpt;
    }

    //fonction qui découvre toutes les cases
    function discoverAll(){
      let liste = document.getElementsByClassName("case");
      for (let i=0; i< liste.length; i++){
        liste[i].disabled = true;
        if (liste[i].cellule.estBombe()){
          liste[i].value = "💣";
        }
        else{
          document.getElementsByClassName("case")[i].value = document.getElementsByClassName("case")[i].cellule.contenu+"";
        }
      }
    }

    //fonction qui permet de placer un drapeau sur une case
    function suspect(button){
      let c = button.cellule;
      c.setSuspect();
      if (c.suspect){
        button.value = "⚑";
        let cpt = document.getElementById("del");
        cpt.bombes -= 1;
        cpt.alt = cpt.bombes+"";
      }
      else{
        button.value = "";
        let cpt = document.getElementById("del");
        cpt.bombes += 1;
        cpt.alt = cpt.bombes+"";
      }
    }

    //fonction qui est activé lorsqu'on clique sur une case, soit elle met un drapeau soit elle découvre la case
    function clicking(button){
      if (document.getElementById("flag").flag){
        suspect(button);
      }
      else{
        discover(button);
      }
    }

    //fonction qui génère le plateau de jeu avec toutes les cases
    function genPlateau(p){
      var plateau = document.createElement("section");
      for (let i = 0; i < p.hauteur; i++){
        var line = document.createElement("div");
        line.className = "rows";
        for (let j = 0; j < p.largeur; j++){
          let btn = document.createElement("input");
          btn.id = "btn"+i+":"+j;
          btn.type = "button";
          btn.classList.add("case");
          btn.line = i;
          btn.column = j;
          btn.style.width = "30pt";
          btn.style.height = "30pt";
          btn.cellule = p.plateau[i][j];
          btn.onclick = function(){clicking(btn);};
          // btn.value = p.plateau[i][j].contenu;
          line.appendChild(btn);
        }
        plateau.appendChild(line);
      }
      document.getElementById("currenttask").appendChild(plateau);
    }

    //fonction qui rafraichit la liste des joueurs présents dans la base de donnée (fichier.json)
    function refreshPlayerList(){
      // $("#currenttask").empty();
      $.ajax({
        url: "http://localhost:3000/player",
        type: "GET",
        dataType : "json",
        success: function(player) {
          console.log(JSON.stringify(player));
          var tab = player.sort(function (a,b) {
            let x = a.scores;
            let y = b.scores;
            return x < y ? -1 : x > y ? 1 : 0;
          });
          $('#taches').empty();
          $('#taches').append($('<ol>'));
          for(var i=0;i<tab.length;i++){
            if (i>=15){
              break;
            }
            console.log(tab[i]);
            $('#taches ol')
            .append($('<li>')
            .append($('<a>')
            .text(tab[i].name+" - "+tab[i].scores)
          ).on("click", tab[i], detailsPlayer)
            );
          }
        },
        error: function(req, status, err) {
          $("#taches").html("<b>Impossible de récupérer les players à afficher !</b>");
        }
      });
    }

    //fonction qui fournit les détails d'un joueur
    function detailsPlayer(event){
        $("#currenttask").empty();
        formPlayer();
        fillFormPlayer(event.data);
    }

    $("#tools #flag").on("click", drapeau);

    //fonction qui affiche un joueur
    function formPlayer(isnew){
        $("#currenttask").empty();
        $("#currenttask")
            .append($('<span>Pseudo<input type="text" id="name" disabled=false><br></span>'))
            .append($('<span><input type="hidden" id="bestTop"><br></span>'))
            .append($('<span><input type="hidden" id="size"><br></span>'))
            .append($('<div ><ol id = "listeScoreJoueur"></ol></div>'))
            .append($('<span><input id="delPlay" type="button" value="Exploser le Joueur"><br></span>'));
        }

      //fonction qui supprime un joueur présent dans la base de donnée (fichier.json)
      function delPlayer(name){
        $.ajax({
          url: "http://localhost:3000/player",
          type: "GET",
          dataType : "json",
          success: function(player) {
            for (let i = 0; i<player.length; i++){
              if (player[i].name == name){
                console.log(player[i].name);
                $.ajax({
                    url: "http://localhost:3000/player/"+player[i].id,
                    type: 'DELETE',
                    contentType: 'application/json',
                    data: JSON.stringify(player[i]),
                    dataType: 'json',
                    success: function (msg) {
                        alert('Save Success');
                    },
                    error: function (err){
                        alert('Save Error');
                    }
                    });

                    refreshPlayerList();
              }
            }
          },
          error: function(req, status, err) {
            $("#taches").html("<b>Impossible de récupérer les players à afficher !</b>");
          }
        });
        $("#currenttask").empty();
      }

    //fonction qui rempli un joueur par les valeurs rentrées sur le site
    function fillFormPlayer(p){
        $("#currenttask #name").val(p.name);
        $.ajax({
          url: "http://localhost:3000/player",
          type: "GET",
          dataType : "json",
          success: function(player) {
            var tab = player.sort(function (a,b) {
              let x = a.scores;
              let y = b.scores;
              return x < y ? -1 : x > y ? 1 : 0;
            });
            for (let i = 0; i<tab.length; i++){
              if (player[i].name == p.name){
                let lvl = i+1;
                $("#listeScoreJoueur").append($('<li>'+player[i].grid_size+" : "+player[i].scores+'. Classement général : '+lvl+'</li>'));
              }
            }
          },
          error: function(req, status, err) {
            $("#taches").html("<b>Impossible de récupérer les players à afficher !</b>");
          }
        });
         $("#delPlay").on("click", function(){delPlayer(p.name);})
         p.uri = (p.uri == undefined) ? "http://localhost:3000/player/"+p.id : p.uri;
         $("#currenttask #puri").val(p.uri);

    }

    //fonction qui enregistre les modifications effectués sur un joueur
    function saveModifiedPlayer(){
        var player = new Player(
          $("#currenttask #name").val(),
          $("#currenttask #scores").val(),
          $("#currenttask #size").val(),
          $("#currenttask #puri").val()
          );
        $.ajax({
            url: player.uri,
            type: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(player),
            dataType: 'json',
            success: function (msg) {
                alert('Save Success');
            },
            error: function (err){
                alert('Save Error');
            }
            });
        refreshPlayerList();
    }

    //fonction qui enregistr un nouveau joueur
    saveNewPlayer = function(){
      let name = $("#currenttask #name").val();
      let score = $("#chronotime").val();
      let size = "";
      if(nbCase < 110){
        size = "10x10";
      }
      else{
        size = "15x15";
      }
      var joueur = new Player(name, score, size);
      $.ajax({
        url: "http://localhost:3000/player",
        type : "POST",
        contentType : "application/json",
        data: JSON.stringify(joueur),
        dataType : "json",
        success : function(msg){
          alert("Save success");
        },
        error : function(msg){
          alert("Save error");
        }
      });
      $("#currenttask").empty();
      refreshPlayerList();
    }


    // function saveNewPlayer(){
    //     var player = new Player(
    //         $("#currenttask #name").val(),
    //         $("#currenttask #scores").val(),
    //         $("#currenttask #size").val(),
    //         );
    //     console.log(JSON.stringify(player));
    //     $.ajax({
    //         url: "http://localhost:3000/player",
    //         type: 'POST',
    //         contentType: 'application/json',
    //         data: JSON.stringify(task),
    //         dataType: 'json',
    //         success: function (msg) {
    //             alert('Save Success');
    //         },
    //         error: function (err){
    //             alert('Save Error');
    //         }
    //         });
    //     refreshPlayerList();
    // }

    /////////////////////////////////////////////////////////////////////////
    //partie chrono qu'on n'arrive pas à mettre dans un autre fichier///////
    ////////////////////////////////////////////////////////////////////////

    var chronometer = document.getElementById("chronotime");
    var start = document.getElementById("startChrono");
    // stop = document.getElementById('stop'),
    var clear = document.getElementById('resetChrono');
    var milliseconds = 0, seconds = 0, minutes = 0;
    var t;
    console.log(chronometer);

    //fonction pour ajouter du temps
    function add() {
        milliseconds++;
        if (milliseconds >= 100){
            milliseconds = 0;
            seconds++;
            if (seconds >= 60) {
                seconds = 0;
                minutes++;
                if (minutes >= 60) {
                    minutes = 0;
                    hours++;
                }
            }
        }

        chronometer.value = (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds) + ":" + (milliseconds > 9 ? milliseconds : "0" + milliseconds);

        timer();
    }

    function timer() {
        t = setTimeout(add, 11); //11 pour compenser le petit retard à cause du temps de calcul
    }

    //fonction pour reset le chrono
    function resetChrono() {
        chronometer.value = "00:00:00";
        milliseconds = 0; seconds = 0; minutes = 0;
    }

    //fonction pour lancer le chrono
    function startChrono() {
      timer();
    }

    //fonction pour stopper le chrono
    function stopChrono() {
      clearTimeout(t);
    }
});
