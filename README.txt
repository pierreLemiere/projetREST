Création projet web avec REST -> Le jeu du démineur:
Le démineur est un jeu où l'on doit découvrir toutes les cases exceptés les bombes.
Pour ne pas tomber sur une bombe il suffit de regarder les chiffres qui apparaissent lorsque l'on découvre les cases.
Les chiffres découverts correspondent au nombre de bombes situées dans les cases voisines.
A vous de JOUER !

Pierre: https://gitlab.com/pierreLemiere/projetREST.git

Maxime: https://gitlab.com/DeboffleMaxime/projetREST.git

Lancement de l'application:

1. Vérifiez que vous possédez bien node.js installé sur votre ordinateur.

2. Dans le dossier de l'application, ouvrez un terminal et exécuter la commande : ./jsonServ/node_modules/json-server/bin/index.js player.json

3. Le serveur est à présent lancé, pour lancer l'application ouvrez le fichier client.html avec votre navigateur

4. Enfin amusez-vous sur notre Démineur :)
