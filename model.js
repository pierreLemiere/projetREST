function Partie(hauteur, largeur, nbBombes){

  this.creationPlateau = function(){
    for (var i=0; i < this.hauteur; i++){
      var line = [];
      for (var j=0; j < this.largeur; j++){
        line.push(new Case(i,j));
      }
      this.plateau.push(line);
    }
  };

  this.creerBombes = function(){
    for (var b = 0; b < this.nbBombes; b++){
      var i = Math.floor(Math.random()*this.hauteur);
      var j = Math.floor(Math.random()*this.largeur);
      if (!this.plateau[i][j].estBombe()){
        this.plateau[i][j].devientBombe();
      }
      else{
        b -= 1;
      }
      console.log(b);
    }
  };

  this.gestionVoisins = function(){
    for (var i = 0; i < this.hauteur; i++){
      for (var j = 0; j < this.largeur; j++){
        var ca = this.plateau[i][j];
        if (i>0){
          if (j>0){
            ca.addVoisin(this.plateau[i-1][j-1]);
          }
          if(j<this.largeur-1){
            ca.addVoisin(this.plateau[i-1][j+1]);
          }
          ca.addVoisin(this.plateau[i-1][j]);
        }
        if (i<this.hauteur-1){
          if (j>0){
            ca.addVoisin(this.plateau[i+1][j-1]);
          }
          if(j<this.largeur-1){
            ca.addVoisin(this.plateau[i+1][j+1]);
          }
          ca.addVoisin(this.plateau[i+1][j]);
        }
        if (j>0){
          ca.addVoisin(this.plateau[i][j-1]);
        }
        if (j<this.largeur-1){
          ca.addVoisin(this.plateau[i][j+1]);
        }
      }
    }
  };



  this.hauteur = hauteur;
  this.largeur = largeur;
  this.nbBombes = nbBombes;
  this.plateau = [];
  this.creationPlateau();
  this.creerBombes();
  this.gestionVoisins();

}

function Case(i,j){

  this.setContenu = function(contenu){
    this.contenu = contenu;
  };

  this.estBombe = function(){
    return this.contenu == -1;
  };

  this.devientBombe = function(){
    this.setContenu(-1);
  };

  this.addVoisin = function(c){
    this.voisins.push(c);
    if (c.estBombe() && !this.estBombe()){
      this.contenu+=1;
    }
  };

  this.discover = function(){
    this.decouvert = true;
  }

  this.setSuspect = function(){
    this.suspect =! this.suspect;
  }


  this.voisins = [];
  this.contenu = 0;
  this.line = i;
  this.column = j;
  this.decouvert = false;
  this.suspect = false;
}

// Object Player en JS
function Player(name, score, grid_size, uri){
  this.name = name;
  this.scores = score;
  this.bestTop = 0;
  this.grid_size = grid_size;
  this.uri = uri;
  console.log(this.uri);
 }
