$(function() {
  var chronometer = document.getElementById("chronotime");
  var start = document.getElementById("startChrono");
  // stop = document.getElementById('stop'),
  var clear = document.getElementById('resetChrono');
  var milliseconds = 0, seconds = 0, minutes = 0;
  var t;
  console.log(chronometer);

  function add() {
      milliseconds++;
      if (milliseconds >= 100){
          milliseconds = 0;
          seconds++;
          if (seconds >= 60) {
              seconds = 0;
              minutes++;
              if (minutes >= 60) {
                  minutes = 0;
                  hours++;
              }
          }
      }

      chronometer.value = (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds) + ":" + (milliseconds > 9 ? milliseconds : "0" + milliseconds);

      timer();
  }

  function timer() {
      t = setTimeout(add, 11); //11 pour compenser le petit retard à cause du temps de calcul
  }


  /* Start button */
  // function startStop(){
  //   if (start.value == "start!"){
  //     timer();
  //     start.value = "stop!";
  //   }
  //   else{
  //     clearTimeout(t);
  //     start.value = "start!";
  //   }
  // }
  // start.onclick = startStop;
  //
  // /* Clear button */
  // clear.onclick = function() {
  //     chronometer.value = "00:00:00";
  //     milliseconds = 0; seconds = 0; minutes = 0;
  // }

  function resetChrono() {
      chronometer.value = "00:00:00";
      milliseconds = 0; seconds = 0; minutes = 0;
  }

  function startChrono() {
    timer();
  }

  function stopChrono() {
    clearTimeout(t);
  }

});
